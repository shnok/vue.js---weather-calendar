export default {
  // Get query string key:val format
  getUrlKey(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [, ""])[1].replace(/\+/g, '%20')) || null;
  },
  // Translate words based on language query string
  tl(word) {
    if(this.getUrlKey('lang') !== 'en') {
      return word;
    }

    switch(word) {
      case '日': return 'Sun';
      case '一': return 'Mon';
      case '二': return 'Tue';
      case '三': return 'Wed';
      case '四': return 'Thu';
      case '五': return 'Fri';
      case '六': return 'Sat';
      case '高温': return 'Hottest';
      case '低温': return 'Coldest';
      case '特殊天气': return 'Bad weather';
      case '今天': return 'Today';
      default: return word;
    }
  },
  // check if now is night or day
  dayOrNight() {
    let hour = new Date().getHours();
    return hour >= 6 && hour <= 18 ? 'd' : 'n';
  }
}
