// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import utils from './utils'

Vue.prototype.$utils = utils
Vue.config.productionTip = false
Vue.prototype.CONFIG = process.env.CUSTOM_CONFIG

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
