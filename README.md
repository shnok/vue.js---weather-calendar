# Calendar-demo

A weather calendar demo made with Vue.js. The UI scales with the page, it's adapted
for a mobile device.

> To set the language to english, add ?lang=en query-string

## Setup and start

> npm install vue-cli -g
> npm install
> npm run dev

*Thomas JULLIEN*
